export const PORT = process.env.API_PORT;
export const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;
