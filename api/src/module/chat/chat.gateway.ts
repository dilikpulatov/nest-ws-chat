import {
	MessageBody,
	OnGatewayConnection,
	OnGatewayDisconnect,
	SubscribeMessage,
	WebSocketGateway,
	WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { Logger } from '@nestjs/common';
import { ChatService } from '@/module/chat/chat.service';
import { ChatItemDto } from '@/module/chat/dto/chat-item.dto';
import { UsersService } from '@/module/users/users.service';

@WebSocketGateway({
	path: '/ws',
	serverClient: true
})
export class ChatGateway implements OnGatewayConnection, OnGatewayConnection, OnGatewayDisconnect {
	constructor(
		private chatService: ChatService,
		private userService: UsersService,
		private logger: Logger = new Logger('ChatGateway')
	) {}

	@WebSocketServer()
	wss: Server;

	afterInit(server: Server) {
		this.logger.log('Init ChatGateway');
	}

	handleDisconnect(client: Socket) {
		this.logger.log(`Client disconnected: ${client.id}`);
	}

	async handleConnection(client: Socket) {
		this.logger.log(`Client connected: ${client.id}`);
		client.emit('connectionSuccess', 'Здарова Чувак!');
		const messages = await this.chatService.getAllMessages();
		client.emit('updateList', messages);
	}

	@SubscribeMessage('sendMessage')
	async sendMessage(@MessageBody() data: ChatItemDto) {
		// this.logger.log(`sendMessage:`, data);
		const message = await this.chatService.setMessage(data);
		const author = await this.userService.findById(message.authorId);
		this.wss.sockets.emit('newMessage', {
			text: message.text,
			fileUrl: message.fileUrl,
			createdAt: message.createdAt,
			authorId: message.authorId,
			author: {
				name: author.name
			}
		});
	}
}
