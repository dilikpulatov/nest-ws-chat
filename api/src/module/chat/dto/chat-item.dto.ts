export class ChatItemDto{
	authorId: number;
	text: string;
	fileUrl?: string;
}
