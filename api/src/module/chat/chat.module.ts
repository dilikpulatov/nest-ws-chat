import { Logger, Module } from '@nestjs/common';
import { ChatService } from './chat.service';
import { ChatGateway } from '@/module/chat/chat.gateway';
import { PrismaModule } from '@/module/prisma/prisma.module';
import { UsersService } from '@/module/users/users.service';

@Module({
  providers: [ChatService, ChatGateway, Logger, UsersService],
  exports: [ChatService],
  imports: [PrismaModule]
})
export class ChatModule {}
