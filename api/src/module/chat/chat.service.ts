import { Injectable } from '@nestjs/common';
import { PrismaService } from '@/module/prisma/prisma.service';
import { ChatItemDto } from '@/module/chat/dto/chat-item.dto';

@Injectable()
export class ChatService {
	constructor(
		private prisma: PrismaService
	) {}

	async getAllMessages(){
		return this.prisma.messages.findMany({
			select: {
				text: true,
				fileUrl: true,
				authorId: true,
				author: {
					select: {
						name: true
					}
				},
				createdAt: true
			}
		});
	}

	async setMessage(message: ChatItemDto){
		return this.prisma.messages.create({data: message});
	}
}
