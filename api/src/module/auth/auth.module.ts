import { Module } from '@nestjs/common';
import { AuthController } from '@/module/auth/auth.controller';
import { AuthService } from '@/module/auth/auth.service';
import { UsersModule } from '@/module/users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { JWT_SECRET_KEY } from '@/config/main';
import { PassportModule } from '@nestjs/passport';

@Module({
	controllers: [AuthController],
	providers: [AuthService],
	imports: [
		PassportModule,
		JwtModule.registerAsync({
			useFactory: () => ({
				secret: JWT_SECRET_KEY
			})
		}),
		UsersModule
	]
})
export class AuthModule {}
