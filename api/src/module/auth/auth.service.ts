import { BadRequestException, HttpException, Injectable, UnauthorizedException } from '@nestjs/common';
import { RegistrationDto } from '@/module/auth/dto/registration.dto';
import { UsersService } from '@/module/users/users.service';
import { User } from '@prisma/client';
import { LoginDto } from '@/module/auth/dto/login.dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
	constructor(
		private readonly usersService: UsersService,
		private readonly jwtService: JwtService
	) {}

	async login(dto: LoginDto): Promise<Pick<User, 'id' | 'username' | 'name'>>{
		const user = await this.usersService.findByUsername(dto.username);
		if (!user) {
			throw new UnauthorizedException(`Пользователь с таким логином "${dto.username}" не найдно.`);
		}
		const isCorrectPassword = await this.usersService.isCorrectPassword(dto.password, user.password);
		if (!isCorrectPassword) {
			throw new UnauthorizedException(`Некорректный логин или пароль.`);
		}
		return {
			id: user.id,
			username: user.username,
			name: user.name
		};
	}

	async generateToken(payload: Pick<User, 'id' | 'username' | 'name'>) {
		return {
			access_token: await this.jwtService.signAsync(payload)
		};
	}

	async registrationNewUser(dto: RegistrationDto){
		const hasUser = await this.usersService.findByUsername(dto.username);
		if (!!hasUser) {
			throw new BadRequestException(`Пользователь с таким логином "${dto.username}" существует.`);
		} else {
			return this.usersService.createUser(dto);
		}
	}
}
