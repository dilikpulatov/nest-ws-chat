import { IsString, IsDefined } from 'class-validator';

export class LoginDto {

	@IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
	@IsString({ message: args => `Значение "${args.property}" должен быть строкой.` })
	username: string;

	@IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
	password: string;
}
