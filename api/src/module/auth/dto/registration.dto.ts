import { IsString, IsDefined, MinLength } from 'class-validator';

export class RegistrationDto {

	@IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
	@IsString({ message: args => `Значение "${args.property}" должен быть строкой.` })
	name: string;

	@IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
	@IsString({ message: args => `Значение "${args.property}" должен быть строкой.` })
	username: string;

	@IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
	@MinLength(6, { message: args => `Минимальная длина пароля 6 символов.` })
	password: string;
}
