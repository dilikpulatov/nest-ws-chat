import {
	Body,
	Controller, HttpCode,
	Post,
	UsePipes,
	ValidationPipe,
} from '@nestjs/common';
import { LoginDto } from '@/module/auth/dto/login.dto';
import { RegistrationDto } from '@/module/auth/dto/registration.dto';
import { AuthService } from '@/module/auth/auth.service';

@Controller('auth')
export class AuthController {
	constructor(
		private readonly authService: AuthService)
	{ }

	@Post('login')
	@HttpCode(200)
	@UsePipes(new ValidationPipe())
	async login(@Body() loginData: LoginDto) {
		const payload = await this.authService.login(loginData);
		return this.authService.generateToken(payload);
	}

	@Post('registration')
	@HttpCode(201)
	@UsePipes(new ValidationPipe())
	registration(@Body() registrationData: RegistrationDto) {
		return this.authService.registrationNewUser(registrationData);
	}
}
