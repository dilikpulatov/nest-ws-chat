import { Controller, HttpCode, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { JwtAuthGuard } from '@/module/auth/guards/jwt.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { FileResponseDto } from '@/module/files/dto/file-response.dto';
import { FilesService } from '@/module/files/files.service';

@Controller('files')
export class FilesController {
	constructor(
		private readonly fileService: FilesService
	) {}

	@Post('upload')
	@HttpCode(200)
	@UseGuards(JwtAuthGuard)
	@UseInterceptors(FileInterceptor('file'))
	// @ts-ignore
	async uploadFile(@UploadedFile() file: Express.Multer.File): Promise<FileResponseDto>{
		return this.fileService.uploadFile(file);
	}
}
