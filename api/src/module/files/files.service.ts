import { Injectable } from '@nestjs/common';
import { FileResponseDto } from '@/module/files/dto/file-response.dto';
import { format } from 'date-fns';
import { path } from 'app-root-path';
import { ensureDir, writeFile } from 'fs-extra';

@Injectable()
export class FilesService {

	async uploadFile(file: Express.Multer.File): Promise<FileResponseDto> {
		const folderPath = format(new Date(), 'yyyy/MM/dd');
		const prefixFile = format(new Date(), 't');
		const createFolder = `${path}/public/uploads/${folderPath}`;
		await ensureDir(createFolder);
		await writeFile(`${createFolder}/${prefixFile}-${file.originalname}`, file.buffer);
		return {
			url: `/uploads/${folderPath}/${prefixFile}-${file.originalname}`,
			name: file.originalname
		};
	}
}
