import { Module } from '@nestjs/common';
import { FilesService } from './files.service';
import { FilesController } from '@/module/files/files.controller';

@Module({
  controllers: [FilesController],
  providers: [FilesService]
})
export class FilesModule {}
