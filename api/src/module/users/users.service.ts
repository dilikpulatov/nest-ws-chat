import { Injectable } from '@nestjs/common';
import { PrismaService } from '@/module/prisma/prisma.service';
import { User } from '@prisma/client';
import { compare, genSalt, hash } from 'bcryptjs';
import { RegistrationDto } from '@/module/auth/dto/registration.dto';

@Injectable()
export class UsersService {
	constructor(
		private prisma: PrismaService
	) {}

	async getAllUsers(): Promise<User[]> {
		return this.prisma.user.findMany();
	}

	async createUser(dto: RegistrationDto): Promise<User> {
		try {
			const salt = await genSalt(12);
			dto.password = await hash(dto.password, salt);
			return this.prisma.user.create({data: dto});
		} catch (e) {
			throw e;
		}
	}

	async findByUsername(username: string): Promise<User> {
		return this.prisma.user.findUnique({
			where: {
				username
			}
		});
	}

	async findById(id: number): Promise<User> {
		return this.prisma.user.findUnique({ where: { id } });
	}

	async isCorrectPassword(password: string, passwordHash: string): Promise<boolean> {
		return compare(password, passwordHash);
	}
}
