import { Controller, Get, HttpCode, UseGuards } from '@nestjs/common';
import { UsersService } from '@/module/users/users.service';
import { JwtAuthGuard } from '@/module/auth/guards/jwt.guard';

@Controller('users')
export class UsersController {
	constructor(
		private readonly usersService: UsersService
	) {}

	@Get()
	@UseGuards(JwtAuthGuard)
	@HttpCode(200)
	async getAllUsers(){
		return this.usersService.getAllUsers();
	}
}
