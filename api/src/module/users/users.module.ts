import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from '@/module/users/users.service';
import { PrismaModule } from '@/module/prisma/prisma.module';

@Module({
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
  imports: [PrismaModule]
})
export class UsersModule {}
