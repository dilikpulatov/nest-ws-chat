import { Logger, Module } from '@nestjs/common';
import { AppController } from '@/app.controller';
import { AppService } from '@/app.service';
import { PrismaModule } from '@/module/prisma/prisma.module';
import { AuthModule } from '@/module/auth/auth.module';
import { JwtStrategy } from '@/module/auth/strategies/jwt.strategy';
import { UsersModule } from '@/module/users/users.module';
import { FilesModule } from './module/files/files.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { path } from 'app-root-path';
import { ChatModule } from './module/chat/chat.module';

@Module({
	imports: [
		PrismaModule,
		AuthModule,
		UsersModule,
		FilesModule,
		ServeStaticModule.forRoot({
			rootPath: `${path}/public`,
			exclude: ['/api*']
		}),
		ChatModule,
		Logger
	],
	exports: [
		PrismaModule,
		UsersModule,
		Logger
	],
	controllers: [AppController],
	providers: [
		AppService,
		JwtStrategy
	],
})
export class AppModule {}
