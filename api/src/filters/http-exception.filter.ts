import {
	ExceptionFilter,
	Catch,
	ArgumentsHost,
	HttpException,
	HttpStatus
} from '@nestjs/common';
import { Prisma } from '@prisma/client';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
	catch(exception: unknown, host: ArgumentsHost) {
		const ctx = host.switchToHttp();
		const response = ctx.getResponse();
		const request = ctx.getRequest();

		// tslint:disable-next-line:no-console
		console.log('HttpExceptionFilter: ', exception);

		if (exception instanceof Prisma.PrismaClientKnownRequestError) {
			response.status(HttpStatus.BAD_REQUEST).json({
				statusCode: HttpStatus.BAD_REQUEST,
				messages: [`${exception.code}: ${exception.name}`],
				timestamp: new Date().toISOString(),
				path: request.url,
			});
		} else {
			const status =
				exception instanceof HttpException
					? exception.getStatus()
					: HttpStatus.INTERNAL_SERVER_ERROR;

			const excRes: any =
				exception instanceof HttpException
					? exception.getResponse()
					: {};

			let messageList: string[];
			if (typeof excRes === 'string') {
				messageList = [excRes];
			} else if (typeof excRes === 'object') {
				if (typeof excRes.message === 'string') {
					messageList = [excRes.message];
				} else if (Array.isArray(excRes.message)) {
					messageList = excRes.message;
				}
			}
			response.status(status).json({
				statusCode: status,
				messages: messageList,
				timestamp: new Date().toISOString(),
				path: request.url,
			});
		}

	}
}
