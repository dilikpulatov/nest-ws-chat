import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './filters/http-exception.filter';
import {PORT} from './config/main';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.setGlobalPrefix('api');
	app.useGlobalFilters(new HttpExceptionFilter());

	await app.listen(PORT);
	console.log(`Api Listen http://localhost:${PORT}`);
}

bootstrap();
