## Running the app

```bash
$ docker-compose up --build -d

$ docker exec -it chat-app-api sh -c "npx prisma migrate dev --name init"

```
